// eslint-disable-next-line import/prefer-default-export
export const theme = {
  color: {
  },
  fontSize: {
    helper: '12px',
    small: '14px',
    regular: '16px',
    title: '18px',
  },
  fontFamily: {
    regular: 'AvenirNext-Regular',
    italic: 'AvenirNext-Italic',
    bold: 'AvenirNext-Bold',
  },
  defaultPadding: '24px',
};
