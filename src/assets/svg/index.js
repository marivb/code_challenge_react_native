import React from 'react';
import Svg, {Path, G, Circle} from 'react-native-svg';

export const Logo = () => (
    <Svg
        width="61"
        height="40"
        viewBox="0 0 61 40"
        fill="none">
        <Circle
            cx="20"
            cy="20"
            r="20" f
            ill="#FFAC30"
        />
        <Path
            d="M60.6453 20C60.6453 25.3043 58.5381 30.3914 54.7874 34.1421C51.0367 37.8929 45.9496 40 40.6453 40C35.3409 40 30.2539 37.8929 26.5031 34.1421C22.7524 30.3914 20.6453 25.3043 20.6453 20L40.6453 20H60.6453Z"
            fill="#3A4276"
        />
    </Svg>
);
